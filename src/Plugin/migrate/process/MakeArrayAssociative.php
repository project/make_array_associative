<?php

namespace Drupal\make_array_associative\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\Exception\BadPluginDefinitionException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * This plugin converts an indexed array of flat values to an indexed array of
 * single-element associative arrays.
 * That is, each flat value of the original array becomes a new associative
 * array, whose key is defined in the plugin configuration and whose value is
 * the original array's value of that position.
 * See the example below.
 *
 * Possible use case: If you have to migrate a multivalue field and pipe it to
 * another process plugin that cannot handle multiple values, you'll need to
 * somehow iterate through the array of values and pipe each one of them to that
 * plugin.
 * The sub_process plugin that is provided by core can only work on an array of
 * associative arrays. So if your source is something like:
 *
 * some_source: Array
 *   (
 *     [0] => "one"
 *     [1] => "two"
 *   )
 *
 * then you cannot use sub_process.
 *
 * This plugin will convert this array to an associative one, with a key that
 * you define in its configuration:
 *
 * @code
 *     field_my_field:
 *       plugin: make_array_associative
 *       key: my_temp_key
 *       source: some_source
 * @endcode
 *
 * Then your value will be converted to:
 *
 * some_source: Array
 *   (
 *     [0] => ["my_temp_key" => "one"]
 *     [1] => ["my_temp_key" => "two"]
 *   )
 *
 * which means that you can now pipe it to the sub_process plugin:
 *
 * @code
 *   field_my_field:
 *     - plugin: make_array_associative
 *       key: my_temp_key
 *       source: some_source
 *     - plugin: sub_process
 *       process:
 *         final_key:
 *           plugin: some_plugin_that_cannot_handle_multiple_values
 *           source: my_temp_key
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "make_array_associative",
 *   handle_multiples = TRUE
 * )
 */
class MakeArrayAssociative extends ProcessPluginBase {

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $configuration = $this->configuration;
    if (!isset($configuration["key"])) {
      throw new BadPluginDefinitionException("make_array_associative", "key");
    }

    if (!is_array($value)) {
      $value = [$value];
    }
    $return = [];
    foreach ($value as $_value) {
      $return[] = [
        $configuration["key"] => $_value,
      ];
    }
    return $return;
  }

  public function multiple() {
    return TRUE;
  }

}
